import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Post } from '../post-list-item-component/Post';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit {
  @Input() list: Array<Post> ;

  constructor() { }

  ngOnInit() {
  }

}
