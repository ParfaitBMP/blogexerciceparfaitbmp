import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Post } from './Post';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.scss']
})
export class PostListItemComponentComponent implements OnInit {
  @Input() post_item: Post;

  constructor() {}

  ngOnInit() {
  }

  onUp() {
    this.post_item.loveIts++;
  }

  onDown() {
    this.post_item.loveIts--;
  }
}
