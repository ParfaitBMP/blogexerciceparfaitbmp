import { Component } from '@angular/core';
import { Post } from './post-list-item-component/Post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  posts_list = [
    // tslint:disable-next-line:max-line-length
    new Post('Un premier post', 'didunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, consequat. Duis aute irure dolor in reprehenderit in'),
    new Post('Un second', 'enim ad minim veniam, consequat. Duis aute irure dolor in reprehenderit in '),
    new Post('Puis troisième', 'ad minim veniam, consequat. Duis aute irure dolor in reprehenderit in '),
    new Post('Et un dernier', 'magna aliqua. Ut enim ad minim veniam, consequat. Duis aute irure dolor in reprehenderit in')
  ];

  constructor() {}
}
